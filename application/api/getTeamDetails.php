<?php
include_once('../resources/inc/header.php');
include_once('../resources/Spielbericht/Team.php');

use \Spielbericht\Team;

if ($_GET && $_GET['team_id']) {
    echo Team::getTeamDetails($_GET['team_id']);
} else {
    echo json_encode('');
}