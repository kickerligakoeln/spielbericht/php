<?php
include_once('../resources/inc/header.php');
require_once('../config.php');


$status = 401;

if (isset($_SERVER["HTTP_ADMIN"]) && $_SERVER["HTTP_ADMIN"] === ADMIN_AUTH) {
  $status = 200;
}

//http_response_code($status); // does not work
echo $status;