<?php
include_once('../resources/inc/header.php');
include_once('../resources/Spielbericht/Statistics.php');

use \Spielbericht\Statistics;

if (isset($_GET["game_type"]) && isset($_GET["season"])) {
    $Statistics = new Statistics($_GET["game_type"], $_GET["season"]);
    echo $Statistics->getAggregatedStatistics();
} else {
    echo json_encode(array());
}