<?php
include_once('../resources/inc/header.php');
require_once('../config.php');
include_once('../resources/Spielbericht/Scoresheet.php');

use \Spielbericht\Scoresheet;

if(isset($_SERVER["HTTP_ADMIN"]) && $_SERVER["HTTP_ADMIN"] === ADMIN_AUTH) {
    if(isset($_GET['match'])) {
        $Scoresheet = new Scoresheet();
        echo $Scoresheet->removeScoresheetById(intval($_GET['match']));
    } else {
        echo "missing match id";
    }
} else {
    echo "incorrect authentification";
}