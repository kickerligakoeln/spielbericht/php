<?php
include_once('../resources/inc/header.php');
include_once('../resources/Spielbericht/Scoresheet.php');
include_once('../resources/Spielbericht/Session.php');

use \Spielbericht\Scoresheet;
use \Spielbericht\Session;

new Session();

$Scoresheet = new Scoresheet();
$result = $Scoresheet->setValidation($_GET["match"]);

if ($result) {
    echo $result;
} else {
    echo 'Could not set a new manager';
}