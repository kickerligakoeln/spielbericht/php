<?php
include_once('../../resources/inc/header.php');
include_once('../../resources/Spielbericht/Statistics.php');

use \Spielbericht\Statistics;

if (isset($_GET["season"])) {
    $Statistics = new Statistics('gameday', $_GET["season"]);
    echo $Statistics->getAggregatedStatistics();
} else {
    echo 'no output';
}