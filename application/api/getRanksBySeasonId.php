<?php
include_once('../resources/inc/header.php');
include_once('../resources/Spielbericht/League.php');

use \Spielbericht\League;

if ($_GET && $_GET['season_id']) {
    echo League::getRanksBySeasonId($_GET['season_id']);
} else {
    echo json_encode('');
}