<?php
include_once('../../resources/Spielbericht/Scoresheet.php');

use \Spielbericht\Scoresheet;

$Scoresheet = new Scoresheet();
$options = array();

if (isset($_GET)) {
    foreach ($_GET as $key => $param) {
        $options[$key] = $param;
    }
}
echo $Scoresheet->getAllScoresheetsByMatchInfo($options);