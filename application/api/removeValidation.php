<?php
include_once('../resources/inc/header.php');
include_once('../resources/Spielbericht/Scoresheet.php');
include_once('../resources/Spielbericht/Session.php');

use \Spielbericht\Scoresheet;
use \Spielbericht\Session;

new Session();

$Scoresheet = new Scoresheet();
$result = $Scoresheet->removeValidation($_GET["match"]);

if ($result > 0) {
    echo "Validation entry has been deleted";
} else {
    echo 'Could not find a validation entry';
}