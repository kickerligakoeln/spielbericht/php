<?php
include_once('../resources/inc/header.php');
include_once('../resources/Spielbericht/Match.php');

use \Spielbericht\Match;

if ($_GET && $_GET['gamedayId']) {
    echo Match::getUpcomingMatchesByGamedayId($_GET['gamedayId']);
} else {
    echo json_encode('');
}