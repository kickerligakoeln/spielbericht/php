<?php
include_once('../resources/inc/header.php');
include_once('../resources/Spielbericht/Scoresheet.php');
include_once('../resources/Spielbericht/Session.php');
include_once('../resources/Spielbericht/Ligatool.php');
include_once('../resources/Spielbericht/Push.php');

use \Spielbericht\Scoresheet;
use \Spielbericht\Session;
use \Spielbericht\Ligatool;
use \Spielbericht\Push;

new Session();

$Scoresheet = new Scoresheet();
$postdata = file_get_contents("php://input");
$data = json_decode($postdata);

if (isset($data->matchid) && isset($data->league)) {
    $push = new Push();
    $result = $Scoresheet->saveScoresheet($data);

    if ($result > 0) {
        if(isset($ENABLE_FEATURE)) {
            if ($ENABLE_FEATURE["send-to-ligatool"]) {
                $ligatool = new Ligatool();
                $ligatool->sendMatch($data);
            }
        }

        try {
            $push->sendNotification($data);
        } catch (Exception $e) {
            // echo $e->errorMessage();
        }

        echo 'safed!';

    } else {
        echo 'Could not save or update the scoresheet.';
    }

} else {
    echo 'Need league and matchid.';
    return;
}