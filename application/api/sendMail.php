<?php
include_once('../resources/inc/header.php');
require_once __DIR__ . '/../resources/Spielbericht/App.php';
require_once __DIR__ . '/../resources/Spielbericht/Mail.php';
require_once __DIR__ . '/../config.php';

use \Spielbericht\App;
use \Spielbericht\Mail;

$Mail = new Mail();

$postdata = file_get_contents("php://input");
$scoresheet = json_decode($postdata);


/**
 * Get Mail Template
 */
$mailTemplate = $Mail->spielbericht($scoresheet);


/**
 * Set Mail Options
 */
$options = array();

$teamHome = $scoresheet->team_home->name;
$teamGuest = $scoresheet->team_guest->name;

if (isset($scoresheet->team_home->mail)) {
    $options["addAddress"] = array(
        $teamHome => $scoresheet->team_home->mail,
        $teamGuest => $scoresheet->team_guest->mail
    );
}

$user = end($scoresheet->user->report);
if (isset($user->admin)) {
    $options["admin"] = true;
}

switch ($scoresheet->gameType) {
    case "cup":
        $options["subject"] = 'Pokalspiel // ' . $teamHome . ' - ' . $teamGuest . ' (' . $scoresheet->team_home->set . ':' . $scoresheet->team_guest->set . ')';
        $options["addAddress"]["Pokalleitung"] = POKAL_MAIL;
        break;
    case "custom":
        $options["subject"] = 'Freundschaftsspiel // ' . $teamHome . ' - ' . $teamGuest;
        break;
    case "gameday":
        $options["subject"] = $scoresheet->matchInfo->gamedayNumber . '. Spieltag // ' . $scoresheet->matchInfo->leagueName . ' // ' . $teamHome . ' - ' . $teamGuest;
        break;
    default:
        $options["subject"] = $teamHome . ' - ' . $teamGuest . ' (' . $scoresheet->team_home->set . ':' . $scoresheet->team_guest->set . ')';
}


$options["customHeader"] = array(
    'X-Spielbericht-MatchId: ' . $scoresheet->matchid,
    'X-Spielbericht-Home: ' . $teamHome,
    'X-Spielbericht-Away: ' . $teamGuest,
    'X-Spielbericht-Score: ' . $scoresheet->team_home->set . '-' . $scoresheet->team_guest->set,
    'X-Spielbericht-Goals: ' . $scoresheet->team_home->goals . '-' . $scoresheet->team_guest->goals,
);

$durationForFinalScoreInSeconds = 30 * 60;
$playingTime = strtotime($scoresheet->date->end) - strtotime($scoresheet->date->create);
if ($playingTime > $durationForFinalScoreInSeconds) {
    $options["customHeader"][] = 'X-Final-Score: true';
}


/**
 * Send Mail
 */
if(isset($ENABLE_FEATURE)) {
    if($ENABLE_FEATURE["send-mail"]) {
      $Mail->send($mailTemplate, $options);
      echo "success";
    }
}