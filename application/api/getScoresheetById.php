<?php
include_once('../resources/inc/header.php');
include_once('../resources/Spielbericht/Scoresheet.php');

use \Spielbericht\Scoresheet;

if (isset($_GET["match"])) {
    $Scoresheet = new Scoresheet();
    echo $Scoresheet->getScoresheetById(intval($_GET['match']));
} else {
    echo 'false';
}
