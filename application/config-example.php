<?php
/**
 * Definiere den Pfad unter dem dein Dienst erreichbar ist
 */
define('REPORT_PATH', 'http://spiel.kickerligakoeln.de/');


/**
 * Mail Adressen bestimmen
 */
define('LIGALEITUNG_MAIL', 'ligaleitung@kickerligakoeln.de');
define('POKAL_MAIL', 'pokal@kickerligakoeln.de');


/**
 * Authentification und API Keys
 *
 * @ADMIN_AUTH
 * admin verification by cookie
 *
 * @KKL_API_URL / @KKL_API_KEY
 * GET public data from KKL API (eg Leagues, Matches...)
 * use api key to get private user data (eg E-Mail)
 */
define('ADMIN_AUTH', 'xxx');
define('KKL_API_URL', 'https://www.kickerligakoeln.de/wp-json/kkl/v1/');
define('KKL_API_KEY', 'xxx');

define('FACEBOOK_ADMIN_ID', 'xxx');
define('GOOGLE_MAPS_API_KEY', 'xxx');
define('ONESIGNAL_APP_ID', 'xxx');
define('ONESIGNAL_AUTH_ID', 'xxx');


/**
 * Toggle Features
 *
 * @send-mail (boolean)
 * send final scoresheets to captains by mail
 * false  for testing mode
 *
 * @push-to-ligatool (boolean)
 * PATCH / PUT scoresheet data to kkl-ligatool
 * by saving scoresheet
 * Attention: You need a valid KKL_API_KEY!
 */
$ENABLE_FEATURE = array(
    "send-mail" => false,
    "send-to-ligatool" => false
);