<?php

namespace Spielbericht;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

/**
 * Class Socket
 * @package Spielbericht
 */
class Socket implements MessageComponentInterface {
  protected $clients;

  /**
   * Socket constructor.
   */
  public function __construct() {
    $this->clients = new \SplObjectStorage;
  }

  /**
   * @param ConnectionInterface $conn
   */
  public function onOpen(ConnectionInterface $conn) {
    $query = $conn->WebSocket->request->getQuery()->urlEncode();
    if(
    isset($query['matchid'])
    ) {
      $this->clients->attach($conn);
      echo "New connection! ({$conn->resourceId})\n";
    } else {
      echo "Connection denied (missing query parameters)";
    }

  }

  /**
   * @param ConnectionInterface $from
   * @param $msg
   */
  public function onMessage(ConnectionInterface $from, $msg) {
    $query = $from->WebSocket->request->getQuery()->urlEncode();
    if($query['matchid']) {
      $this->sendUpdate($query['matchid'], $msg);
    }
  }

  /**
   * @param ConnectionInterface $conn
   */
  public function onClose(ConnectionInterface $conn) {
    $this->clients->detach($conn);

    echo "Connection {$conn->resourceId} has disconnected\n";
  }

  /**
   * @param ConnectionInterface $conn
   * @param \Exception $e
   */
  public function onError(ConnectionInterface $conn, \Exception $e) {
    echo "An error has occurred: {$e->getMessage()}\n";

    $conn->close();
  }

  /**
   * @param $matchid
   * @param $updates string JSON string with all the updates
   */
  public function sendUpdate($matchid, $updates) {
    foreach($this->clients as $client) {
      $query = $client->WebSocket->request->getQuery()->urlEncode();
      if(
          $query['matchid'] === $matchid
      ) {
        $client->send($updates);
      }
    }
  }
}