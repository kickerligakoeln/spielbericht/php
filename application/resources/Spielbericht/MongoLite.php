<?php

namespace Spielbericht;
include_once(__DIR__ . '/Database.php');

use MongoLite\Client;

require __DIR__ . '/../../vendor/autoload.php';

class MongoLite implements Database {
    const DATABASE_PATH = __DIR__ . '/../../database';

    private $collection_type = array("gameday", "cup", "custom");
    private $collection;

    public function __construct() {
        $client = new Client(self::DATABASE_PATH);
        $database = $client->KKL_DB;

        $this->collection = array(
            "validation" => $database->validation,
            "gameday" => $database->gameday,
            "cup" => $database->cup,
            "custom" => $database->custom
        );
    }


    /**
     * fetch all "ended" scoresheets for gameType / season
     *
     * @param $gameType
     * @param $season
     * @return mixed|string
     */
    public function getAllRegularScoresheets($gameType, $season) {
        $response = $this->collection[$gameType]->find(function ($document) use ($season) {
            $create = $document["date"]["create"];
            $pos = strpos($create, $season);
            $totalScore = $document["team_guest"]["set"] + $document["team_home"]["set"];

            if ($pos !== false && $totalScore == 20) {
                return $document["date"]["create"];
            } else {
                return false;
            }
        });

        return $response;
    }


    /**
     * fetch all scoresheets for gameType / season
     *
     * @param $gameType
     * @param $season
     * @return mixed
     */
    public function getAllScoresheets($gameType, $season) {
        $response = $this->collection[$gameType]->find(function ($document) use ($season) {
            $create = $document["date"]["create"];
            $pos = strpos($create, $season);

            if ($pos !== false) {
                return $document["matchid"];
            } else {
                return false;
            }
        });

        return $response;
    }


    /**
     * @return string
     */
    public function getAllLiveGames() {
        $output = [];

        foreach ($this->collection_type as $gameType) {
            $response = $this->collection[$gameType]->find(function ($document) {
                $create = $document["date"]["create"];
                $today = date("Y-m-d");
                $pos = strpos($create, $today);

                if ($pos !== false) {
                    return $document["date"]["create"];
                } else {
                    return false;
                }
            });

            foreach ($response as $item) {
                $item["live"] = true;
                $date = substr($item["date"]["create"], 5, 2) . '-' . substr($item["date"]["create"], 8, 2);
                $output[$gameType][$date][] = $item;

                krsort($output);
            }
        }

        return json_encode($output, true);
    }


    /**
     * @param $gameType
     * @param $options
     * @return string
     */
    public function findScoresheets($gameType, $options) {
        $response = $this->collection[$gameType]->find($options);

        return $response;
    }


    /**
     * @param $matchId
     * @return array|mixed
     */
    public function getScoresheetById($matchId) {
        $output = false;

        foreach ($this->collection_type as $gameType) {
            $response = $this->collection[$gameType]->findOne(["matchid" => $matchId]);

            if ($response) {
                $output = $response;
            }
        }

        return json_encode($output, true);
    }


    /**
     * @param $document
     * @return mixed last_insert_id for single document or
     * count of inserted documents for arrays
     */
    public function setScoresheet($document) {
        $scoresheet = json_encode($document);
        $gametype = $document->gameType;

        return $this->collection[$gametype]->insert(
            json_decode($scoresheet, true)
        );
    }


    /**
     * @param $document
     * @return integer
     */
    public function updateScoresheet($document) {
        $scoresheet = json_encode($document);
        $gametype = $document->gameType;
        $matchid = $document->matchid;

        return $this->collection[$gametype]->update(
            ["matchid" => $matchid],
            json_decode($scoresheet, true)
        );
    }


    /**
     * @param $matchId
     * @return string
     */
    public function removeScoresheetById($matchId) {
        $output = 0;

        foreach ($this->collection_type as $gameType) {
            $output += $this->collection[$gameType]->remove(["matchid" => $matchId]);
        }

        $output += $this->collection["validation"]->remove(["matchid" => $matchId]);

        return $output;
    }


    /**
     * @param $authentification
     * @return mixed
     */
    public function setValidation($authentification) {
        return $this->collection["validation"]->insert($authentification);
    }


    /**
     * @param $authentification
     * @return mixed
     */
    public function getValidation($authentification) {
        $output = false;

        $response = $this->collection["validation"]->find([
            "matchid" => $authentification["matchid"],
            "session" => $authentification["session"]
        ]);

        foreach ($response as $item) {
            $output[] = $item;
        }

        return $output;
    }


    /**
     * @param $authentification
     * @return string
     */
    public function removeValidation($authentification) {
        $output = $this->collection["validation"]->remove(["matchid" => $authentification["matchid"]]);

        return $output;
    }
}