<?php

namespace Spielbericht;

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../../config.php';

use Twig_Loader_Filesystem;
use Twig_Environment;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mail {
    const MAIL_PATH = __DIR__ . "/../../mail";

    protected $config;
    protected $app;

    public function __construct() {
        global $ENABLE_FEATURE;

        $this->app = new App();
        $this->config =& $ENABLE_FEATURE;

        $loader = new Twig_Loader_Filesystem(self::MAIL_PATH . "/templates");
        $this->twig = new Twig_Environment($loader, array(
            'debug' => true,
            'cache' => self::MAIL_PATH . "/cache",
        ));
    }


    /**
     * get scoresheet mail template
     *
     * @param $scoresheet
     * @return mixed
     */
    public function spielbericht($scoresheet) {
        return $this->twig->render("spielbericht.html", array("scoresheet" => $scoresheet));
    }


    /**
     * send Mail
     *
     * @param $mailTemplate
     * @param $options
     */
    public function send($mailTemplate, $options) {
        $mail = new PHPMailer(true);

        try {
            $mail->CharSet = "UTF-8";
            $mail->AddReplyTo(LIGALEITUNG_MAIL, 'KKL Ligaleitung');
            $mail->SetFrom(LIGALEITUNG_MAIL, 'KKL Ligaleitung');

            $mail->AddAddress(LIGALEITUNG_MAIL, 'KKL Ligaleitung');

            if (!$this->app->isDev() && $this->config["send-mail"] && !$options["admin"]) {
                foreach ($options["addAddress"] as $name => $mailAddress) {
                    $mail->AddAddress($mailAddress, $name);
                }
            }

            foreach ($options["customHeader"] as $customHeader) {
                $mail->addCustomHeader($customHeader);
            };

            $mail->Subject = $options["subject"];

            $mail->IsHTML(true);
            $mail->AltBody = 'Leider kann Ihre E-Mail-Software keine HTML-Mails darstellen!';
            $mail->MsgHTML($mailTemplate);
            $mail->Send();

        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage(); //Boring error messages from anything else!
        }
    }
}