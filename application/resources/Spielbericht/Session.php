<?php

namespace Spielbericht;

/**
 * Class Session
 * @package Spielbericht
 */
class Session {

    public function __construct() {
        session_start();
        if (!$_SESSION['id']) {
            $_SESSION['id'] = uniqid();
        }
    }
}