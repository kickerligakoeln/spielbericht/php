<?php
/**
 * Created by IntelliJ IDEA.
 * User: stephan
 * Date: 21.02.17
 * Time: 12:55
 */


namespace Spielbericht;

include_once(__DIR__ . '/League.php');
include_once(__DIR__ . '/../../config.php');

use OneSignal\Config;
use OneSignal\OneSignal;


require dirname(__DIR__) . '/../vendor/autoload.php';


class Push {

    private $api;

    /**
     * Push constructor.
     */
    public function __construct() {

        $config = new Config();
        $config->setApplicationId(ONESIGNAL_APP_ID);
        $config->setApplicationAuthKey(ONESIGNAL_AUTH_ID);

        $this->api = new OneSignal($config);

    }

    public function sendNotification($scoresheet) {
        // Do not combine targeting parameters
        $this->api->notifications->add([
            'template_id' => '56d61aca-1b5f-437b-8096-bbe84a774cfc',
            'contents' => [
                'de' => $scoresheet->team_home->name . ' vs. ' . $scoresheet->team_guest->name . ' (' . $scoresheet->team_home->set . ":" . $scoresheet->team_guest->set . ')',
                'en' => $scoresheet->team_home->name . ' vs. ' . $scoresheet->team_guest->name . ' (' . $scoresheet->team_home->set . ":" . $scoresheet->team_guest->set . ')'
            ],
            'headings' => [
                'de' => 'Kölner Kickerliga | ' . League::getLeagueFullName($scoresheet->league),
                'en' => 'Kölner Kickerliga | ' . League::getLeagueFullName($scoresheet->league)
            ],
            'url' => 'http://spiel.kickerligakoeln.de/live.php?match=' . $scoresheet->matchid,

            'filters' => [
                [
                    "field" => "tag",
                    "key" => "matchid_" . $scoresheet->matchid,
                    "relation" => "=",
                    "value" => 'kkl'
                ]
            ],
            'included_segments' => ['All']
        ]);
    }
}
