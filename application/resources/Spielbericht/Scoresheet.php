<?php

namespace Spielbericht;
include_once(__DIR__ . '/../../config.php');
include_once(__DIR__ . '/MongoLite.php');

/**
 * Class Scoresheet
 * @package Spielbericht
 */
class Scoresheet {

  private $db;

  public function __construct() {
    $this->db = new MongoLite();
  }


  /**
   * @param $scoresheet
   * @return bool
   */
  private function isLive($scoresheet) {
    $live = false;
    $today = date("Y-m-d");
    $pos = strpos($scoresheet["date"]["create"], $today);
    if($pos !== false) {
      $live = true;
    }

    return $live;
  }


  /**
   * check for admin cookie
   *
   * @return bool
   */
  private function isAdmin() {
    return (isset($_SERVER["HTTP_ADMIN"]) && $_SERVER["HTTP_ADMIN"] === ADMIN_AUTH);
  }


  /**
   * Get All finalized Scoresheets
   * admin gets all scoresheets
   *
   * @param $gameType
   * @param $season
   * @return string
   */
  public function getAllScoresheetsForSeason($gameType, $season) {
    $output = [];

    if($this->isAdmin()) {
      $response = $this->db->findScoresheets($gameType, function($document) use ($season) {
        $create = $document["date"]["create"];
        $pos = strpos($create, $season);

        if($pos !== false) {
          return $document["date"]["create"];
        } else {
          return false;
        }
      });
    } else if($gameType === "cup") {
      $response = $this->db->findScoresheets($gameType, function($document) use ($season) {
        $create = $document["date"]["create"];
        $pos = strpos($create, $season);

        if($pos !== false) {
          return $document["date"]["create"];
        } else {
          return false;
        }
      });
    } else {
      $response = $this->db->getAllRegularScoresheets($gameType, $season);
    }


    foreach($response as $item) {
      $item["live"] = $this->isLive($item);
      $gameday = '0';
      if(isset($item["gameday"])) {
        $gamedayNumber = str_pad($item["gameday"], 2, '0', STR_PAD_LEFT); // with leading zero
        $gameday = $gamedayNumber . ". Spieltag";
      }
      $date = substr($item["date"]["create"], 5, 2) . '-' . substr($item["date"]["create"], 8, 2);

      $output[$gameday][$date][] = $item;
      krsort($output[$gameday]);
    }

    krsort($output);
    return json_encode($output, true);
  }


  /**
   * @param $options
   * @return false|string
   */
  public function getAllScoresheetsByMatchInfo($options) {
    $output = [];

    if(sizeof($options) > 0) {
      $response = $this->db->findScoresheets("gameday", function($document) use ($options) {
        $optionFlag = false;
        foreach($options as $option => $value) {
          $matchinfo = $document["matchInfo"];

          if(isset($matchinfo) && isset($matchinfo[$option]) && $matchinfo[$option] == $value) {
            $optionFlag = true;
          }
        }
        if($optionFlag) {
          return $document["matchid"];
        } else {
          return false;
        }
      });

      foreach($response as $item) {
        $output[] = $item;
        krsort($output);
      }
    }

    return json_encode($output, true);
  }


  /**
   * @param $options
   * @return mixed
   */
  public function getAllLiveGames($options) {

    if(sizeof($options) > 0) {
      $output = [];

      /** @var array $response */
      $response = $this->db->findScoresheets("gameday", function($document) use ($options) {
        $season = false;
        if(isset($document["matchInfo"]["currentSeason"])) $season = $document["matchInfo"]["currentSeason"] == $options["season"];
        $gameday = $document["gameday"] == $options["gameday"];
        $league = $document["league"] == $options["league"];

        return $gameday && $league && $season;
      });

      foreach($response as $item) {
        $item["live"] = $this->isLive($item);

        $output["gameday"][$options["gameday"]][] = $item;
        krsort($output);
      }
      return json_encode($output, true);

    } else {
      return $this->db->getAllLiveGames();
    }
  }


  /**
   * @param $matchId
   * @return string
   */
  public function getScoresheetById($matchId) {
    return $this->db->getScoresheetById($matchId);
  }


  /**
   * Save / Update Scoresheet in File
   *
   * find scoresheet entry in database
   * - "no" entry in db, set validation of current user
   * - entry in db, authenticate current user and update scoresheet
   *
   * @param $scoresheet
   * @return mixed
   */
  public function saveScoresheet($scoresheet) {
    $validation = $this->getValidation($scoresheet->matchid);

    if($validation) {
      $returnSheet = $this->db->getScoresheetById($scoresheet->matchid);

      if($returnSheet === "false") {
        return $this->db->setScoresheet($scoresheet);
      } else {
        return $this->db->updateScoresheet($scoresheet);
      }

    } else {
      return "Can not update file because of incorrect authentication";
    }

  }


  /**
   * remove scoresheet
   *
   * @param $matchId
   * @return array|mixed
   */
  public function removeScoresheetById($matchId) {
    return $this->db->removeScoresheetById($matchId);
  }


  /**
   * Create Validation File for authentification
   *
   * @param $matchId
   * @return mixed
   */
  public function setValidation($matchId) {
    $editor_auth = $_SESSION["id"];
    $authentification = array(
        "matchid" => $matchId,
        "timestamp" => date('Y-m-d H:i:s'),
        "session" => $editor_auth
    );

    // setcookie("EDITOR_AUTH", $editor_auth, 0, "/"); // deprecated

    return ($this->db->setValidation($authentification) > 0) ? $editor_auth : null;
  }


  /**
   * @param $matchId
   * @return mixed
   */
  public function getValidation($matchId) {

    if(isset($_SERVER["HTTP_EDITOR"])) {
      $authentification = array(
          "matchid" => $matchId,
          "session" => $_SERVER["HTTP_EDITOR"]
      );
      return $this->db->getValidation($authentification);
    } else {
      return false;
    }
  }


  /**
   * @param $matchId
   * @return mixed
   */
  public function removeValidation($matchId) {
    if(isset($_SERVER["HTTP_EDITOR"])) {
      $authentification = array(
          "matchid" => $matchId,
          "session" => $_SERVER["HTTP_EDITOR"] || false
      );

      return $this->db->removeValidation($authentification);
    } else {
      return false;
    }
  }
}