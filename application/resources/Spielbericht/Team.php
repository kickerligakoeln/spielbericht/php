<?php

namespace Spielbericht;

include_once(__DIR__ . '/../../config.php');

/**
 * Class Team
 * @package Spielbericht
 */
class Team {

    /**
     * @param $teamId
     * @return bool|string
     */
    public static function getTeamDetails($teamId) {
        $context = stream_context_create([
            "http" => [
                "method" => "GET",
                "header" => "X-Api-Key: " . KKL_API_KEY . "\r\n"
            ]
        ]);
        $teamDetails = file_get_contents(KKL_API_URL . 'teams/' . $teamId, false, $context);
        if ($teamDetails) {
            return $teamDetails;
        } else {
            return false;
        }
    }

}