<?php

namespace Spielbericht;
include_once(__DIR__ . '/JSONFile.php');
include_once(__DIR__ . '/MongoLite.php');

/**
 * Class Statistics
 * @package Spielbericht
 */
class Statistics {
    const SCORESHEET_PATH = __DIR__ . '/../../public/scoresheet/';

    private $db;
    private $gameType;
    private $season;

    public function __construct($gameType, $season) {
        $this->db = new MongoLite();

        $this->gameType = $gameType;
        $this->season = $season;
    }


    /**
     * @return string
     */
    public function getAggregatedStatistics() {
        $output = array(
            "games" => array(
                "total" => $this->getGamesCount()
            ),
            "goals" => array(
                "total" => $this->getTotalScore("goals"),
                "home" => $this->getTotalScoreBySide("goals", "team_home"),
                "guest" => $this->getTotalScoreBySide("goals", "team_guest")
            ),
            "sets" => array(
                "total" => $this->getTotalScore("set"),
                "home" => $this->getTotalScoreBySide("set", "team_home"),
                "guest" => $this->getTotalScoreBySide("set", "team_guest")
            ),
            "players" => array(
                "total" => $this->getTotalPlayers(),
                "home" => $this->getTotalPlayersBySide("team_home"),
                "guest" => $this->getTotalPlayersBySide("team_guest"),
                "details" => $this->getMostPlayerNames()
            ),
            "time" => array(
                "total" => $this->getTotalTime()
            ),
            "teams" => $this->getTeamOverview(),
            "userAgent" => $this->getUserAgent()
        );

        return json_encode($output, true);
    }


  /**
   * @return array
   */
    public function getMostPlayerNames() {
      $output = array();
      $allRegularScoresheets = $this->db->getAllRegularScoresheets($this->gameType, $this->season);

      foreach($allRegularScoresheets as $scoresheet) {
        $team_type = array(
            "team_home",
            "team_guest"
        );


        // check player stats per scoresheet
        $playerStats = array();
        foreach($scoresheet['matches'] as $match) {
          $player_a = strtolower($match['player']['home_a']);
          $player_b = strtolower($match['player']['home_b']);
          $player_c = strtolower($match['player']['guest_a']);
          $player_d = strtolower($match['player']['guest_b']);

          $score_home = ($match['score'][0]['home'] + $match['score'][1]['home']);
          $score_guest = ($match['score'][0]['guest'] + $match['score'][1]['guest']);

          if(!isset($playerStats[$player_a])) {
            $playerStats[$player_a] = array("win" => $score_home, "loose" => $score_guest);
          } else {
            $playerStats[$player_a]["win"] = $playerStats[$player_a]["win"] + $score_home;
            $playerStats[$player_a]["loose"] = $playerStats[$player_a]["loose"] + $score_guest;
          }

          if(!isset($playerStats[$player_b])) {
            $playerStats[$player_b] = array("win" => $score_home, "loose" => $score_guest);
          } else {
            $playerStats[$player_b]["win"] = $playerStats[$player_b]["win"] + $score_home;
            $playerStats[$player_b]["loose"] = $playerStats[$player_b]["loose"] + $score_guest;
          }

          if(!isset($playerStats[$player_c])) {
            $playerStats[$player_c] = array("win" => $score_home, "loose" => $score_guest);
          } else {
            $playerStats[$player_c]["win"] = $playerStats[$player_c]["win"] + $score_guest;
            $playerStats[$player_c]["loose"] = $playerStats[$player_c]["loose"] + $score_home;
          }

          if(!isset($playerStats[$player_d])) {
            $playerStats[$player_d] = array("win" => $score_home, "loose" => $score_guest);
          } else {
            $playerStats[$player_d]["win"] = $playerStats[$player_d]["win"] + $score_guest;
            $playerStats[$player_d]["loose"] = $playerStats[$player_d]["loose"] + $score_home;
          }
        }


        // get player details
        foreach ($team_type as $team) {
          $team_players = isset($scoresheet[$team]["player"]) ? $scoresheet[$team]["player"] : array();

          foreach($team_players as $playerItem){
            $player = strtolower($playerItem['name']);

            if(!isset($output[$player])){
              $output[$player] = array(
                  "name" => $player,
                  "amount" => 1,
                  "goals" => $playerStats[$player]
              );
            } else {
              $output[$player]["amount"]++;
              $output[$player]["goals"]["win"] += $playerStats[$player]["win"];
              $output[$player]["goals"]["loose"] += $playerStats[$player]["loose"];
            }

            $output[$player]["goals"]["avarage"] = ($output[$player]["goals"]["win"] * 100) / ($output[$player]["goals"]["win"] + $output[$player]["goals"]["loose"]);
          }
        }
      }

      usort($output, function($a, $b) {
        $filter = "amount";
        return $b[$filter] <=> $a[$filter];
      });

      return $output;
    }


    /**
     * @return string
     */
    public function getGamesCount() {
        $allRegularScoresheets = $this->db->getAllRegularScoresheets($this->gameType, $this->season);

        return $allRegularScoresheets->count();
    }


    /**
     * @param $type
     * @return array
     */
    public function getTotalScore($type) {
        $output = array();
        $total = 0;
        $allRegularScoresheets = $this->db->getAllRegularScoresheets($this->gameType, $this->season);

        foreach ($allRegularScoresheets as $scoresheet) {
            $total += $scoresheet["team_guest"][$type];
            $total += $scoresheet["team_home"][$type];
        }

        if ($allRegularScoresheets->count() > 0) {
            $output = array(
                "total" => $total,
                "avarage" => $total / $allRegularScoresheets->count()
            );
        }

        return $output;
    }


    /**
     * @param $type
     * @param $side
     * @return array
     */
    public function getTotalScoreBySide($type, $side) {
        $output = array();
        $total = 0;
        $allRegularScoresheets = $this->db->getAllRegularScoresheets($this->gameType, $this->season);

        foreach ($allRegularScoresheets as $scoresheet) {
            $total += $scoresheet[$side][$type];
        }

        if ($allRegularScoresheets->count() > 0) {
            $output = array(
                "total" => $total,
                "avarage" => $total / $allRegularScoresheets->count()
            );
        }

        return $output;
    }


    /**
     * @return array
     */
    public function getTotalPlayers() {
        $output = array();
        $total = 0;
        $allRegularScoresheets = $this->db->getAllRegularScoresheets($this->gameType, $this->season);

        foreach ($allRegularScoresheets as $scoresheet) {
            $total += count($scoresheet["team_guest"]["player"]);
            $total += count($scoresheet["team_home"]["player"]);
        }

        if ($allRegularScoresheets->count() > 0) {
            $output = array(
                "total" => $total,
                "avarage" => $total / $allRegularScoresheets->count()
            );
        }

        return $output;
    }


    /**
     * @param $side
     * @return array
     */
    public function getTotalPlayersBySide($side) {
        $output = array();
        $total = 0;
        $allRegularScoresheets = $this->db->getAllRegularScoresheets($this->gameType, $this->season);

        foreach ($allRegularScoresheets as $scoresheet) {
            $total += count($scoresheet[$side]["player"]);
        }

        if ($allRegularScoresheets->count() > 0) {
            $output = array(
                "total" => $total,
                "avarage" => $total / $allRegularScoresheets->count()
            );
        }

        return $output;
    }


    /**
     * @return array
     */
    public function getTotalTime() {
        $output = array();
        $total = array();
        $allRegularScoresheets = $this->db->getAllRegularScoresheets($this->gameType, $this->season);

        foreach ($allRegularScoresheets as $scoresheet) {
            if (isset($scoresheet["date"]["end"])) {
                $timeStart = strtotime($scoresheet["date"]["create"]);
                $timeEnd = strtotime($scoresheet["date"]["end"]);
                array_push($total, $timeEnd - $timeStart);
            }
        }

        if ($allRegularScoresheets->count() > 0) {
            arsort($total);
            $medianKey = ceil(count($total) / 2);
            $median = $total[intval($medianKey)];

            $totalHours = floor(($median * $allRegularScoresheets->count()) / 3600);
            $totalMinutes = floor(($median * $allRegularScoresheets->count()) / 60 % 60);
            $totalSeconds = floor(($median * $allRegularScoresheets->count()) % 60);
            $avgHours = floor($median / 3600);
            $avgMinutes = floor($median / 60 % 60);
            $avgSeconds = floor($median % 60);

            $output = array(
                "total" => sprintf('%02d:%02d:%02d', $totalHours, $totalMinutes, $totalSeconds),
                "avarage" => sprintf('%02d:%02d:%02d', $avgHours, $avgMinutes, $avgSeconds)
            );
        }

        return $output;
    }


    /**
     * @return array
     */
    public function getTeamOverview() {
        $output = array();
        $allScoresheets = $this->db->getAllScoresheets($this->gameType, $this->season);

        foreach ($allScoresheets as $scoresheet) {
            $team_type = array("team_home", "team_guest");

            foreach ($team_type as $team) {
                $team_name = $scoresheet[$team]["name"];
                $team_id = isset($scoresheet[$team]["id"]) ? $scoresheet[$team]["id"] : "-";
                $team_players = isset($scoresheet[$team]["player"]) ? $scoresheet[$team]["player"] : array();

                if (!isset($output[$team_name])) {
                    $output[$team_name] = array(
                        "id" => $team_id,
                        "games" => 1,
                        "name" => $team_name,
                        "players" => array(
                            "total" => count($team_players),
                            "avarage" => count($team_players)
                        )
                    );
                } else {
                    $output[$team_name]["games"]++;
                    $output[$team_name]["players"]["total"] += count($team_players);
                    $output[$team_name]["players"]["avarage"] = $output[$team_name]["players"]["total"] / $output[$team_name]["games"];
                    $output[$team_name]["id"] = $team_id;
                }
            }
        }

        usort($output, function ($a, $b) {
            return $b["players"]["avarage"] <=> $a["players"]["avarage"];
        });

        return $output;
    }


    /**
     * parse_user_agent()
     * -> https://github.com/donatj/PhpUserAgent
     *
     * @return array
     */
    public function getUserAgent() {
        $output = array();
        $allScoresheets = $this->db->getAllScoresheets($this->gameType, $this->season);

        foreach ($allScoresheets as $scoresheet) {
            foreach ($scoresheet["user"]["report"] as $report) {
                $ua_info = parse_user_agent($report["userAgent"]);

                if(!isset($output[$ua_info["platform"]][$ua_info["browser"]]["amount"])){
                    $output[$ua_info["platform"]][$ua_info["browser"]]["amount"] = 1;
                } else {
                    $output[$ua_info["platform"]][$ua_info["browser"]]["amount"]++;
                }


                if (!isset($output[$ua_info["platform"]][$ua_info["browser"]]["ua"][$ua_info["version"]])) {
                    $ua_info["amount"] = 1;
                    $output[$ua_info["platform"]][$ua_info["browser"]]["ua"][$ua_info["version"]] = $ua_info;
                } else {
                    $output[$ua_info["platform"]][$ua_info["browser"]]["ua"][$ua_info["version"]]["amount"]++;
                }
            }
        }


        foreach ($output as $platformKey => $browser) {
            foreach ($browser as $browserKey => $version) {
                usort($version["ua"], function ($a, $b) {
                    return $b["amount"] <=> $a["amount"];
                });

                $output[$platformKey][$browserKey] = $version;
            }
        }

        return $output;
    }
}