<?php

namespace Spielbericht;

include_once(__DIR__ . '/../../config.php');

/**
 * Class Ligatool
 * @package Spielbericht
 */
class Ligatool {

    public function __construct() {

    }

    /**
     * @param $scoresheet
     */
    public function sendMatch($scoresheet) {
        if (isset($scoresheet->team_home)) {

            $sendData = array(
                "goals_home" => $scoresheet->team_home->goals,
                "goals_away" => $scoresheet->team_guest->goals,
                "score_home" => $scoresheet->team_home->set,
                "score_away" => $scoresheet->team_guest->set,
                "external_url" => REPORT_PATH . "/live?match=" . $scoresheet->matchid
            );

            $data = json_encode($sendData);
            $url = KKL_API_URL . "matches/" . $scoresheet->matchid . "/score";
            $headers = array(
                "X-Api-Key:" . KKL_API_KEY,
                "Content-Type:application/json",
                "Content-Length:" . strlen($data)
            );

            $type = (isset($scoresheet->date->end)) ? "PUT" : "PATCH";

            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

            $response = curl_exec($curl);
            curl_close($curl);
        }

    }
}