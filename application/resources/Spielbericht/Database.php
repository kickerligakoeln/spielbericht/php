<?php

namespace Spielbericht;

interface Database {

    /**
     * @param $gameType
     * @param $season
     * @return mixed
     */
    public function getAllRegularScoresheets($gameType, $season);

    /**
     * @param $gameType
     * @param $season
     * @return mixed
     */
    public function getAllScoresheets($gameType, $season);

    /**
     * @param $matchId
     * @return mixed
     */
    public function getScoresheetById($matchId);

    /**
     * @param $authentification
     * @return mixed
     */
    public function setValidation($authentification);

    /**
     * @param $authentification
     * @return mixed
     */
    public function getValidation($authentification);

    /**
     * @param $authentification
     * @return mixed
     */
    public function removeValidation($authentification);

    /**
     * @param $document
     * @return mixed
     */
    public function setScoresheet($document);

    /**
     * @param $document
     * @return mixed
     */
    public function updateScoresheet($document);
}