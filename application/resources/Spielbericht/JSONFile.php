<?php

namespace Spielbericht;
include_once(__DIR__ . '/Database.php');

class JSONFile implements Database {
    const SCORESHEET_PATH = __DIR__ . '/../../scoresheets/';

    /**
     * Get All Scoresheets >> sorted by GameDay
     * @return array
     */
    public function getAllRegularScoresheets($gameType, $year) {
        $files = scandir(self::SCORESHEET_PATH);
        $jsonFiles = [];
        foreach ($files as $file) {
            if (strpos($file, '.json')) {
                $jsonFiles[] = $file;
            }
        }

        rsort($jsonFiles, SORT_NUMERIC);
        $output = [];

        foreach ($jsonFiles as $file) {
            $json = json_decode(file_get_contents(self::SCORESHEET_PATH . $file));
            if ($json->gameType === $gameType) {
                $jsonYear = substr($json->date->create, 0, 4);

                if ($jsonYear === $year) {
                    $date = substr($json->date->create, 5, 2) . '-' . substr($json->date->create, 8, 2);
                    $gameday = '0';
                    if ($json->gameday) {
                        $gameday = str_pad($json->gameday, 2, '0', STR_PAD_LEFT) . '. Spieltag';
                    }

                    $output[$gameday][$date][] = $json;
                    krsort($output[$gameday]);
                }
            }
        }

        krsort($output);
        return json_encode($output, true);
    }


    /**
     * @param $gameType
     * @param $season
     * @return array|mixed
     */
    public function getAllScoresheets($gameType, $season) {
        return $this->getAllRegularScoresheets($gameType, $season);
    }


    /**
     * @return string
     */
    public function getAllLiveGames() {
        $files = scandir(self::SCORESHEET_PATH);
        $jsonFiles = [];
        foreach ($files as $file) {
            if (strpos($file, '.json')) {
                $jsonFiles[] = $file;
            }
        }

        rsort($jsonFiles, SORT_NUMERIC);
        $output = [];

        foreach ($jsonFiles as $file) {
            $json = json_decode(file_get_contents(self::SCORESHEET_PATH . $file));
            $today = date("Y-m-j");
            $pos = strpos($json->date->create, $today);

            if ($pos !== false) {
                $date = substr($json->date->create, 5, 2) . '-' . substr($json->date->create, 8, 2);

                $output[$json->gameType][$date][] = $json;
            }
        }

        krsort($output);
        return json_encode($output, true);
    }


    /**
     * @param $matchId
     * @return string
     */
    public function getScoresheetById($matchId) {
        $path = self::SCORESHEET_PATH . strval($matchId) . '.json';

        if (file_exists($path)) {
            $theMatch = file_get_contents($path);
            return $theMatch;
        } else {
            return '{}';
        }
    }


    /**
     * @param $authentification
     * @return mixed
     */
    public function setValidation($authentification) {
        $saveFile = file_put_contents(self::SCORESHEET_PATH . 'validation/' . $authentification["matchid"] . '.json', json_encode($authentification));

        if ($saveFile !== false) {
            return 1;
        } else {
            return 0;
        }
    }


    /**
     * @param $authentification
     * @return mixed
     */
    public function getValidation($authentification) {
        $path = self::SCORESHEET_PATH . 'validation/' . $authentification["matchid"] . '.json';

        if (file_exists($path)) {
            $validationFile = file_get_contents($path);
            return $validationFile;
        } else {
            return false;
        }
    }


    /**
     * @param $authentification
     * @return mixed
     */
    public function removeValidation($authentification) {
        $path = self::SCORESHEET_PATH . 'validation/' . $authentification["matchid"] . '.json';

        unlink($path);
        return true;
    }


    /**
     * Save / Update Scoresheet in File
     *
     * @param $scoresheet
     * @return mixed
     */
    public function setScoresheet($scoresheet) {
        $matchId = $scoresheet->matchid;
        $saveFile = file_put_contents(self::SCORESHEET_PATH . $matchId . '.json', json_encode($scoresheet));

        if ($saveFile !== false) {
            // return array($saveFile, $scoresheet);
            return 1;
        } else {
            return 0;
        }
    }


    /**
     * @param $scoresheet
     * @return mixed
     */
    public function updateScoresheet($scoresheet) {
        $matchId = $scoresheet->matchid;
        $saveFile = file_put_contents(self::SCORESHEET_PATH . $matchId . '.json', json_encode($scoresheet));

        if ($saveFile !== false) {
            // return array($saveFile, $scoresheet);
            return 1;
        } else {
            return 0;
        }
    }
}