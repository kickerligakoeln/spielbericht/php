<?php

namespace Spielbericht;

require_once __DIR__ . '/../../vendor/autoload.php';
include_once(__DIR__ . '/../../config.php');

class App {

    public function __construct() {
    }

    // TODO: check for environment by config.php
    public function isEnvironment($env) {
        return (strpos(REPORT_PATH, 'localhost') || $_GET['dev']);
    }


    /**
     * @return bool
     */
    public function isDev() {
        return (strpos(REPORT_PATH, 'localhost') || $_GET['dev']);
    }
}