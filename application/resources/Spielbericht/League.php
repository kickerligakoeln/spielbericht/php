<?php

namespace Spielbericht;

include_once(__DIR__ . '/../../config.php');

/**
 * Class League
 * @package Spielbericht
 */
class League {

    /**
     * @return bool|string
     */
    public static function getActiveLeagues() {
        $leagues = file_get_contents(KKL_API_URL . 'leagues?active=true&fields=id,code,name,currentSeason&embed=currentseason');
        if ($leagues) {
            return $leagues;
        } else {
            return false;
        }
    }

    /**
     * @param $season_id
     * @return bool|string
     */
    public static function getRanksBySeasonId($season_id) {
        $ranks = file_get_contents(KKL_API_URL . 'seasons/' . $season_id . '/liveranking?embed=team');
        if ($ranks) {
            return $ranks;
        } else {
            return false;
        }
    }

    /**
     * @param $league
     * @return bool|string
     */
    // TODO: Umbenennen und verschieben ?
    public static function getLeagueFullName($league) {
        $leagues = [
            "koeln1" => "1. Liga",

            "koeln2a" => "2. Liga A",
            "koeln2b" => "2. Liga B",

            "koeln3a" => "3. Liga A",
            "koeln3b" => "3. Liga B",
            "koeln3c" => "3. Liga C",

            "koeln4a" => "4. Liga A",
            "koeln4b" => "4. Liga B",
            "koeln4c" => "4. Liga C",
            "koeln4d" => "4. Liga D",

            "gameday" => "Ligaspiel",
            "custom" => "Freundschaftsspiel",
            "cup" => "Pokalspiel"
        ];

        if ($leagues[$league]) {
            return $leagues[$league];
        } else {
            return $league;
        }
    }

}