<?php

namespace Spielbericht;

include_once(__DIR__ . '/../../config.php');

/**
 * Class Location
 * @package Spielbericht
 */
class Location {

    /**
     * @return bool|string
     */
    public static function getLocations() {
        $locations = file_get_contents(KKL_API_URL . 'locations');

        if ($locations) {
            return $locations;
        } else {
            return false;
        }
    }
}