<?php

namespace Spielbericht;

include_once(__DIR__ . '/../../config.php');

/**
 * Class Match
 * @package Spielbericht
 */
class Match {

    /**
     * @param $gamedayId
     * @return bool|string
     */
    public static function getUpcomingMatchesByGamedayId($gamedayId) {
        $matches = file_get_contents(KKL_API_URL . 'gamedays/' . $gamedayId . '/matches?embed=homeTeam,awayTeam,location,gameDay');
        if ($matches) {
            return $matches;
        } else {
            return false;
        }
    }

    /**
     * @param $gamedayId
     * @param $matchId
     * @return bool
     */
    public static function matchExists($gamedayId, $matchId) {
        $matches = self::getUpcomingMatchesByGamedayId($gamedayId);
        if ($matches) {
            $matches = json_decode($matches);
            foreach ($matches as $match) {
                if ($match->id === $matchId) {
                    return true;
                }
            }

            return false;
        } else {
            return false;
        }
    }


    /**
     * Used for migration
     *
     * @param $matchId
     * @return array
     */
    public static function getMatchDetailsByMatchId($matchId) {
        $jsonMatchDetails = file_get_contents(KKL_API_URL . 'matches/' . $matchId . '/info?fields=id,gameDayId,league');
        $matchDetails = json_decode($jsonMatchDetails, true);

        $jsonSeasonDetails = file_get_contents(KKL_API_URL . 'gamedays/' . $matchDetails['gameDayId']);
        $gamedayDetails = json_decode($jsonSeasonDetails, true);

        return array(
            'matchDetails' => $matchDetails,
            'gamedayDetails' => $gamedayDetails
        );
    }
}