<?php

include_once('../resources/Spielbericht/Scoresheet.php');
include_once('../resources/Spielbericht/Mail.php');

use \Spielbericht\Scoresheet;
use \Spielbericht\Mail;

if(isset($_GET["matchid"])) {
  $Scoresheet = new Scoresheet();
  $scoresheet = $Scoresheet->getScoresheetById(intval($_GET["matchid"]));

  if($scoresheet != 'false') {
    $Mail = new Mail();
    echo $Mail->spielbericht(json_decode($scoresheet));

  } else {
    echo "Can not find Scoresheet by Match ID: <strong>" . $_GET["matchid"] . "</strong>";
  }
} else {
  echo "Match ID is missing.";
}