# Digitaler Spielberichtsbogen
**the application layer**

## Requirements
* [Docker](https://www.docker.com/) v2.0.0.2
* [Composer](https://getcomposer.org/) v1.3.2

#### setup environment
```bash
$ cp docker/example.env .env
$ cp application/config-example.php application/config.php
```

#### build application
```bash
$ cd application/
$ composer install
$ cd ..
```

#### start nginx
```bash
$ docker-compose up --build
```

#### import database dump 
```bash
$ wget -O application/database/KKL_DB.sqlite http://spiel.kickerligakoeln.de/resources/db/KKL_DB.sqlite
```

find your web service here: http://localhost:8888/api/status.php

---

#### backup database dump

```bash
$ mv application/database/KKL_DB.sqlite application/database/$(date +%Y-%m-%d)_KKL_DB.sqlite
$ wget -O application/database/KKL_DB.sqlite http://spiel.kickerligakoeln.de/resources/db/KKL_DB.sqlite
```

#### test web services

```bash
$ cp docs/REST/http-client.env.json docs/REST/http-client.private.env.json
```

```text
execute HTTP Requests directly in your IDEA:

* docs/REST/Web-Rest-API_v1.0.http
* docs/REST/Scoresheet-Rest-API_v1.0.http
* docs/REST/Ligatool-Rest-API_v1.0.http
```